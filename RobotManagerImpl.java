package youngsters2;

public class RobotManagerImpl implements RobotManager {

	private Robot m_robot;
	private Vec2 m_target;
	
	public RobotManagerImpl(Robot robot, Vec2 target)
	{
		m_robot = robot;
		m_target = target;
	}
	
	public Vec2 getTarget() { return m_target; }
	
	@Override
	public RobotCommand getNextCommand()
	{
		// algorithm
	}
	
	@Override
	public boolean sendCommand(RobotCommand command)
	{
		if (command == null)
			return false;
		
		m_robot.execute(command);
		return true;
	}
	
	@Override
	public boolean isComplete()
	{
		Vec2 robotPosition = m_robot.getPosition();
		Vec2 robotDirection = m_robot.getDirection();
		
		return Vec2.isAligned(m_target, robotPosition, robotDirection);
	}
	
}
